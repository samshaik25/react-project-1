import React, { Component } from 'react';
import Counters from './components/counters';
import Navbar from './components/navbar';
import './App.css';
import react from 'react';

class App extends Component {
  state={
    counters:[
    {id:1,value:0},
    {id:2,value:0},
    {id:3,value:0},
    {id :4,value:0},
    {id :5,value:0}
    ]
}
handleIncrement=(counter)=>{
 const  counters=[...this.state.counters]
 const index=  counters.indexOf(counter)
 counters[index].value++;
 this.setState({counters})
}
handleDelete=(id)=>{
   const   counters=this.state.counters.filter(c=>c.id!=id)
      this.setState({counters})
}
handleDecrement=(counter)=>{
   const counters=[...this.state.counters]
   const index=counters.indexOf(counter)
   if(counters[index].value!=0)
   {
       counters[index].value--;
   }
   this.setState({counters})
}

handleReset=()=>{
    const counters=this.state.counters.map(c=>{
      c.value=0;
      return c;
    })
    this.setState({counters})
}
  render() { 
    return (
      <React.Fragment>
      <Navbar totalCounters={this.state.counters.filter(c=>c.value>0).length}/>
      <main className="container">
      <Counters
       counters={this.state.counters}
      onIncrement={this.handleIncrement}
      onDecrement={this.handleDecrement}
      onDelete={this.handleDelete}
      onReset={this.handleReset}
      />
      </main>
      </React.Fragment>
    

    )
  }
}
 
export default App;
